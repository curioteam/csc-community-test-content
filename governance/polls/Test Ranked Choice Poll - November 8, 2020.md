---
title: Test Ranked Choice Poll - November 8, 2020
summary: Test Ranked Choice Poll
discussion_link: https://curioinvest.com
vote_type: Ranked Choice IRV
options:
   0: Abstain
   1: Choice 1
   2: Choice 2
   3: Choice 3
   4: Choice 4
   5: Choice 5
   6: Choice 6
   7: Choice 7
   8: Choice 8
   9: Choice 9
   10: Choice 10
   11: Choice 11
---
# Poll: Test Ranked Choice Poll - November 8, 2020
This is test ranked choice IRV poll of Curio Stablecoin Governance.
