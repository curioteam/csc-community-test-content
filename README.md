# Curio Stablecoin Community - test content

Content base of Curio Community for test cases.

## Resources

### [Governance](governance/README.md)

The test proposals of Curio Stablecoin system's management.
